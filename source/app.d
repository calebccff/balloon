import std.stdio;

import dsfml.graphics;

RenderWindow window;

void draw(){
	
}

void main() {
	dsfmlInitWindow(window);
	while (window.isOpen()) {
		// check all the window's events that were triggered since the last iteration of the loop
		Event event;
		while (window.pollEvent(event)) {
			if (event.type == Event.EventType.Closed) {
				window.close();
			}
		}
		window.clear(Color(150, 150, 255, 255));
		draw();
		window.display();
  	}
}

void dsfmlInitWindow(ref RenderWindow window) {
	window = new RenderWindow(VideoMode(1280, 720),//VideoMode.getFullscreenModes()[0],
		"Balloon",
		Window.Style.None);
  	window.setFramerateLimit(60);
}